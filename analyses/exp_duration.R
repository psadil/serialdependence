library(tidyverse)

d <- fs::dir_ls(
  path = here::here("events", "maskedclick"),
  recurse = TRUE,
  glob = "*meta.csv",
  type = "file") %>%
  vroom::vroom() %>%
  mutate(
    duration = (exp_end - exp_start) / 60) 

summary(d$duration)
