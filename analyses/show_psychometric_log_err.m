function show_psychometric_log_err(quest_data)

min_db = 1.2;
max_db = 1.4;

%% Find out QUEST+'s estimate of the stimulus parameters, obtained
% on the gridded parameter domain.
psiParamsIndex = qpListMaxArg(quest_data.posterior);
psiParamsQuest = quest_data.psiParamsDomain(psiParamsIndex,:);
fprintf('Max posterior QUEST+ parameters: %0.3f, %0.3f, %0.3f, %0.3f\n', ...
    psiParamsQuest(1),psiParamsQuest(2),psiParamsQuest(3),psiParamsQuest(4));

%% Find aximum likelihood fit.  Use psiParams from QUEST+ as the starting
% parameter for the search, and impose as parameter bounds the range
% provided to QUEST+.
% psiParamsFit = qpFit(quest_data.trialData,quest_data.qpPF,psiParamsQuest,quest_data.nOutcomes,...
%     'lowerBounds', [min_db 0 0.5 0],'upperBounds',[max_db 5 0.5 0.04]);
% fprintf('Maximum likelihood fit parameters: %0.1f, %0.1f, %0.1f, %0.2f\n', ...
%     psiParamsFit(1),psiParamsFit(2),psiParamsFit(3),psiParamsFit(4));


%% Plot of trial locations with maximum likelihood fit
figure; clf; hold on
stimCounts = qpCounts(qpData(quest_data.trialData),quest_data.nOutcomes);
stim = [stimCounts.stim];

stimFine0 = linspace(min_db, max_db, 500)';
stimFine_lin = (10 .^ stimFine0) / 20;
stimFine = 20 * log10(stimFine_lin);

plotProportionsFit = qpPFNormal2(stimFine,psiParamsQuest);
for cc = 1:length(stimCounts)
    nTrials(cc) = sum(stimCounts(cc).outcomeCounts);
    pCorrect(cc) = stimCounts(cc).outcomeCounts(2)/nTrials(cc);
end
for cc = 1:length(stimCounts)
    h = scatter(stim(cc),pCorrect(cc),100,'o','MarkerEdgeColor',[0 0 1],'MarkerFaceColor',[0 0 1],...
        'MarkerFaceAlpha',nTrials(cc)/max(nTrials),'MarkerEdgeAlpha',nTrials(cc)/max(nTrials));
end
plot(stimFine,plotProportionsFit(:,2),'-','Color',[1.0 0.2 0.0],'LineWidth',3);
xlabel('Contrast (dB)');
ylabel('Proportion Correct');
ylim([0 1]);
drawnow;

end