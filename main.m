function  main(varargin)
ip = inputParser;
addParameter(ip, 'subject', 99, @isnumeric);
addParameter(ip, 'session', 1, @isnumeric);
addParameter(ip, 'debuglevel', 0, @(x) x == 1 | x == 10 | x == 0);
addParameter(ip, 'skipsynctests', 0, @(x) any(x == 0:2));
addParameter(ip, 'root', pwd, @ischar);
addParameter(ip, 'gamma', true, @islogical);
parse(ip,varargin{:});
input = ip.Results;
addpath(genpath('lib'));


%%  setup
PsychDefaultSetup(2);

%% run experiment
serialdependence = SerialDependence(input);
serialdependence.run();

%% cleanup

% need to delete while all methods available
delete(serialdependence)

rmpath(genpath('lib'));

end