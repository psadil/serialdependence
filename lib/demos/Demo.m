classdef Demo < handle
    % DEMO
    % the difference between a demo and an experiment is that demos will
    % not generally have a data object associated with them. 
    
    properties(Constant)
        skipsynctests = 2
    end
    
    properties
        debuglevel = 0
        window
        grating
    end
    
    methods
        function obj = Demo(varargin)            
            obj.window = Window(varargin{1});
            obj.grating = Grating();
        end
        
        function run(obj)            
            open(obj.window, obj.skipsynctests, obj.debuglevel);
            vbl = prepare(obj.grating, obj.window);
            run(obj.grating, obj.window, 'vbl', vbl);
        end        
    end
end

