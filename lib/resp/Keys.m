classdef Keys < handle
    %Keys Summary of this class goes here
    %   Detailed explanation goes here
        
    properties
        start
        task
    end
    
    methods
        function obj = Keys()
            codes = zeros(1,256);            
            obj.start = codes;
            obj.start(KbName({'space'})) = 1;
            obj.task = codes;
            obj.task(KbName({'left_mouse','right_mouse'})) = 1;
        end        
    end
    
end

