classdef InputHandler < handle
    %InputHandler receive and parse inputs of various kinds from different
    %sources
    
    properties
        keys_pressed = []
        keys = Keys()
        device
    end
    
    properties (SetObservable, AbortSet)
        % allow for listening of press_times so that experiment object
        % can update response + rt (press_times is set after
        % keys_pressed in function check_keys)
        press_times = 0
    end
    
    methods
        function obj = InputHandler()
            % Get first mouse device:
            d = GetMouseIndices;
            obj.device = d(1);
        end
        
        prepare(obj, codes)
        shutdown(obj)
        when = wait_for_start(obj)
        check_keys(obj);
    end
    
    
end

