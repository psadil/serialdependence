function shutdown(obj)
KbQueueStop(obj.device);
KbQueueFlush(obj.device);
KbQueueRelease(obj.device);
end
