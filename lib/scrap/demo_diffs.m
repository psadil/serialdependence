
PsychDefaultSetup(2);
skipsynctests = 2;
debuglevel = 0;

window = Window();
grating = Grating();
grating.contrast = 0.1;

open(window, skipsynctests, debuglevel);
vbl = prepare(grating, window);
run2(grating, window, 'vbl', vbl, 'diff', 2);

imageArray = Screen('GetImage', window.pointer);

sca;
