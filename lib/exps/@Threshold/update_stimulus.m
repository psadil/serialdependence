function update_stimulus(obj, ~, ~)

obj.stimulus.orientation = obj.data.orientation(obj.trial);
obj.stimulus.contrast = obj.data.contrast(obj.trial);

if obj.trial > 1
    % close and create new noise tex for next trial
    Screen('Close', obj.stimulus.tex_noise);
    noiseimg = rand(floor(obj.stimulus.radius_pix*2), floor(obj.stimulus.radius_pix*2));
    obj.stimulus.tex_noise = Screen('MakeTexture', obj.window.pointer, noiseimg);    
end

end
