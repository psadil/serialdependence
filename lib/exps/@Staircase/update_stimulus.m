function update_stimulus(obj, ~, ~)

obj.stimulus.orientation = obj.data.orientation(obj.trial);

if obj.trial == 1
    obj.stimulus.contrast = obj.data.contrast(obj.trial);
else
    % Update quest data structure based on response
    % note that the QUEST+ psychometric function operates on the db scale
    % 1 db = 20*log10(x)
    % also, outcomes are stored such that 1 := correct, 2 := incorrect
    obj.quest_data = qpUpdate(obj.quest_data, obj.stimulus.contrast_db, obj.data.correct(obj.trial-1)+1);
    
    % set contrast for next trial, returning to contrast scale
    obj.stimulus.contrast_db = qpQuery(obj.quest_data);
    obj.stimulus.contrast = 10 ^ (obj.stimulus.contrast_db / 20);
    
    % close and create new noise tex for next trial
    Screen('Close', obj.stimulus.tex_noise);
    noiseimg = rand(floor(obj.stimulus.radius_pix*2), floor(obj.stimulus.radius_pix*2));
    obj.stimulus.tex_noise = Screen('MakeTexture', obj.window.pointer, noiseimg);    
end

end
