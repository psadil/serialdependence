
classdef Staircase < Experiment
    
    properties(Constant)        
        % maximum difference between response and true angle to be counted
        % as correct (actual comparison is < margin, not <=)
        margin_deg = 25 
    end
    
    properties
        exp = 'staircase'
        filenameprefix
        data
        n_trial    
        n_trial_per_block
        stimulus
        
        % QUEST+ params
        quest_data = qpInitialize('stimParamsDomainList',{-40:0.5:-5}, ...
            'psiParamsDomainList',{-40:.2:-10, .05:.01:2, 25/180, 0},...
            'qpPF', @qpPFNormal2);    
    end   
    
    methods
        function obj = Staircase(input)
            obj@Experiment(input);
            
            obj.filenamechecker();
            
            obj.input_handler = InputHandler();
            
            obj.stimulus = Grating();
            
            obj.data = prepare_data(obj);
            obj.n_trial = size(obj.data,1); 
            % assumes blocks all of equal length
            obj.n_trial_per_block = sum(obj.data.block == 1); 
            
            % listen for entry in to new phase of trial
            addlistener(obj.stimulus, 'phase', 'PostSet',...
                @(src,event)obj.log_phase_change(src,event) );
        end
        
        
        function delete(obj)
            if ~isempty(obj.filenameprefix)   
                quest_data_to_save = obj.quest_data; 
                save([obj.filenameprefix,'_quest.mat'], '-struct', 'quest_data_to_save');
            end
            
            % turn off warning after warned about events have occurred (in
            % the preceeding if statement)
            warning('off', 'MATLAB:structOnObject');            
        end

                
        % methods defined in external files
        record_response(obj, ~, ~)
        record_task_event(obj,~,~)
        update_stimulus(obj, ~, ~)
        data = prepare_data(obj)
    end
end

