function thanks(obj, vbl)


msg = ['The experiment is over! \n\n', ...
    'Thanks for participating \n\n', ...
    'Please go find the researcher'];

DrawFormattedText(obj.window.pointer, msg, 'center', 'center');
vbl = Screen('Flip', obj.window.pointer, vbl + obj.stimulus.flip_now_slack*9);

Screen('Flip', obj.window.pointer, vbl + obj.thanks_sec);

end

