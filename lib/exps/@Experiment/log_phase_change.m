function log_phase_change(obj, ~, ~)

switch obj.stimulus.phase
    case 'warning'
        obj.data.warning_on(obj.trial) = obj.stimulus.last_vbl;        
        obj.data.warning_onset(obj.trial) = obj.stimulus.onset;        
        obj.data.warning_fliptime(obj.trial) = obj.stimulus.fliptime;        
        obj.data.warning_missed(obj.trial) = obj.stimulus.missed;        
        obj.data.warning_beam(obj.trial) = obj.stimulus.beam;  
    case 'target'
        obj.data.target_on(obj.trial) = obj.stimulus.last_vbl;
        obj.data.target_onset(obj.trial) = obj.stimulus.onset;        
        obj.data.target_fliptime(obj.trial) = obj.stimulus.fliptime;        
        obj.data.target_missed(obj.trial) = obj.stimulus.missed;        
        obj.data.target_beam(obj.trial) = obj.stimulus.beam;  
    case 'blank'
        obj.data.blank_on(obj.trial) = obj.stimulus.last_vbl;
        obj.data.blank_onset(obj.trial) = obj.stimulus.onset;        
        obj.data.blank_fliptime(obj.trial) = obj.stimulus.fliptime;        
        obj.data.blank_missed(obj.trial) = obj.stimulus.missed;        
        obj.data.blank_beam(obj.trial) = obj.stimulus.beam;  
    case 'probe'
        obj.data.probe_on(obj.trial) = obj.stimulus.last_vbl;
        obj.data.probe_onset(obj.trial) = obj.stimulus.onset;        
        obj.data.probe_fliptime(obj.trial) = obj.stimulus.fliptime;        
        obj.data.probe_missed(obj.trial) = obj.stimulus.missed;        
        obj.data.probe_beam(obj.trial) = obj.stimulus.beam;  
    case 'feedback'
        obj.data.feedback_on(obj.trial) = obj.stimulus.last_vbl;
        obj.data.feedback_onset(obj.trial) = obj.stimulus.onset;        
        obj.data.feedback_fliptime(obj.trial) = obj.stimulus.fliptime;        
        obj.data.feedback_missed(obj.trial) = obj.stimulus.missed;        
        obj.data.feedback_beam(obj.trial) = obj.stimulus.beam;  
        
        % response will already be set by the time the flip has occured
        obj.data.response(obj.trial) = obj.stimulus.response;
        
        % need to check kbqueue to get when the response happened
        obj.input_handler.check_keys();
        
        % record how long it took to make that response
        obj.data.rt(obj.trial) = max(obj.input_handler.press_times) - obj.data.probe_on(obj.trial);
        
    case 'isi'
        obj.data.isi_on(obj.trial) = obj.stimulus.last_vbl;        
        obj.data.isi_onset(obj.trial) = obj.stimulus.onset;        
        obj.data.isi_fliptime(obj.trial) = obj.stimulus.fliptime;        
        obj.data.isi_missed(obj.trial) = obj.stimulus.missed;        
        obj.data.isi_beam(obj.trial) = obj.stimulus.beam;  
end

end

