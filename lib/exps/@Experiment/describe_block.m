function vbl = describe_block(obj, vbl)

if (obj.data.trial(obj.trial) == 1 && obj.data.section(obj.trial) == 1) || ...
        (obj.data.section(obj.trial) ~= obj.data.section(obj.trial - 1))
    
    if (obj.data.section(obj.trial) == 1) && (obj.data.block(obj.trial) == 1)
        msg = sprintf(['You are about to start part 1 out of %d \n\n', ...
            'Click the mouse when you are ready'], max(obj.data.block)*max(obj.data.section));
    else
        last_block_dur = minutes(seconds(obj.data.isi_on(obj.trial-1) - ...
            obj.data.warning_on(obj.trial-obj.n_trial_per_block)));
        c = clock;
        msg = sprintf(['You are about to start part %d out of %d \n\n', ...
            'The last block took about %2.3g minutes \n', ...
            'The current time is %02d:%02d \n\n', ...
            'If you need to, please take a brief break before continuing \n\n', ...
            'Click the mouse when you are ready'], ...
            obj.data.section(obj.trial) + max(obj.data.section)*(obj.data.block(obj.trial) - 1),...
            max(obj.data.block)*max(obj.data.section), last_block_dur, c(4), c(5));
    end
    
    DrawFormattedText(obj.window.pointer, msg, 'center', 'center');
    
    Screen('Flip', obj.window.pointer, vbl + obj.stimulus.flip_now_slack*9);
    obj.input_handler.wait_for(obj.input_handler.keys.task);
    
    obj.stimulus.draw_fixation(obj.window);
    vbl = Screen('Flip', obj.window.pointer, GetSecs + obj.stimulus.flip_now_slack*9);

    obj.stimulus.draw_fixation(obj.window);
    vbl= Screen('Flip', obj.window.pointer, vbl + obj.after_break_sec);

end

end

