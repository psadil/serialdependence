classdef Experiment < handle
    %Experiment Abstract collection of info about general experiment
    
    properties
        savedir
        subject
        session
        
        seed
        input
        datetaken = datetime('now')
        version = Experiment.get_version_string('version.txt')
        matlabversion = version
        psychtoolboxversion = PsychtoolboxVersion
        comp = computer
        
        % moment when Experiment object is being created
        exp_start = GetSecs
        % moment when Experiment object is being deleted
        exp_end = NaN
        after_break_raw_sec = 2
        thanks_raw_sec = 20
        after_break_sec
        thanks_sec
        
        window
        input_handler
        
        exp
    end
    
    properties (Abstract)
        data
        filenameprefix
        n_trial
        stimulus
    end
    
    properties (SetObservable, AbortSet)
        trial = 0
    end
    
    methods
        function obj = Experiment(input, exp)
            
            % saving involves converting to a struct, so we turn this
            % warning off for duration of experiment. delete method will
            % turn the warning back on
            warning('off', 'MATLAB:structOnObject');
            
            if nargin > 0
                rng('shuffle');
                scurr = rng; % set up and seed the randon number generator
                obj.seed = scurr.Seed;
                
                obj.input = input;
                obj.subject = input.subject;
                obj.session = input.session;
                obj.exp = exp;
                
                % setup save filepath info
                obj.savedir = fullfile(obj.input.root, 'events');
                if ~exist(obj.savedir, 'dir')
                    mkdir(obj.savedir);
                end
                
                obj.filenamechecker();
                
                % gather demographics now that there is a filename
                if (input.skipsynctests == 0) && (input.debuglevel == 0)
                    obj.demographics(obj.filenameprefix);
                end
                
                % create and open window
                obj.window = Window(input.gamma);
                open(obj.window, input.skipsynctests, input.debuglevel);
                obj.after_break_sec = obj.after_break_raw_sec - obj.window.slack*obj.window.ifi;
                obj.thanks_sec = obj.thanks_raw_sec - obj.window.slack*obj.window.ifi;
                
                obj.input_handler = InputHandler();
                
            end
            % listener will update properties of stimulus (e.g., contrast
            % or orientation) when the trial increments
            addlistener(obj, 'trial', 'PostSet',...
                @(src,event)obj.update_stimulus(src,event) );
        end
        
        % NOTE: although super convenient to be able to auto delete this
        % object when it's done, making sure to save it and the data, this
        % could easily bite you! It must be the case that the
        % filenameprefix property is only assigned to a value that won't
        % overwrite important files!
        function delete(obj)
            if ~isempty(obj.filenameprefix)
                obj.exp_end = GetSecs;
                
                writetable(as_table(obj), [obj.filenameprefix,'_meta.csv']);
                writetable(finalize_data(obj), [obj.filenameprefix,'_data.csv']);
                writetable(struct2table(struct(obj.stimulus),'AsArray',true),...
                    [obj.filenameprefix,'_stimulus.csv']);
                writetable(struct2table(struct(obj.window),'AsArray',true),...
                    [obj.filenameprefix,'_window.csv']);
                writetable(struct2table(struct(obj.input),'AsArray',true),...
                    [obj.filenameprefix,'_input.csv']);
            end
            
            % turn off warning after warned about events have occurred (in
            % the preceeding if statement)
            warning('off', 'MATLAB:structOnObject');
            
        end
        
        out = finalize_data(obj)
        out = as_table(obj)
        click_time = describe_block(obj, vbl)
        filenamechecker(obj)
        log_phase_change(obj, src, event)
        thanks(obj, vbl)
        
        run(obj)
    end
    
    methods (Abstract)
        update_stimulus(obj, ~, ~)
    end
    
    methods (Access = private, Static = true)
        demographics(directory)
        version = get_version_string(fname, fallback)
    end
end

