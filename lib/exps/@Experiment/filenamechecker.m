function filenamechecker(obj)

% possible filenameprefix to test out
filenameprefix = sprintf('sub-%02d_ses-%02d_task-%s', obj.subject, obj.session, obj.exp);

listing = dir(obj.savedir);

tmp = cellfun(@(x) any(strfind(x,filenameprefix)), {listing.name}, 'UniformOutput', false);

if any(arrayfun(@(x) x{1}==true, tmp))
    error('---files already exist with that prefix---');
else
    obj.filenameprefix = fullfile(obj.savedir, filenameprefix);
end

end

