function out = as_table(obj)
out = table();
out.savedir = obj.savedir;
out.subject = obj.subject;
out.seed = obj.seed;
out.exp_end = obj.exp_end;
out.exp_start = obj.exp_start;
out.datetaken = obj.datetaken;
out.version = obj.version;
end