function run(obj)

% prepare grating stimulus for drawing
vbl = prepare(obj.stimulus, obj.window);

% main experimental loop
for trial = 1:obj.n_trial
        
    % updating obj.trial triggers listener, Experiment.update_stimulus()
    obj.trial = trial;

    % when changing blocks, give participant progress report
    vbl = obj.describe_block(vbl);
    
    % input_handler will look for mouse clicks only
    obj.input_handler.prepare(obj.input_handler.keys.task);
    vbl = run(obj.stimulus, obj.window, 'vbl', vbl + obj.data.isi_sec(trial));
    obj.input_handler.shutdown();
    
    
end

% closing screen thanking participant and instructing them to go find the
% researcher. lingers on screen for a bit.
obj.thanks(vbl);

end
