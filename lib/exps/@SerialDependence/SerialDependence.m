
classdef SerialDependence < Experiment
        
    properties
        filenameprefix
        data
        n_trial    
        n_trial_per_block
        stimulus        
    end   
    
    methods
        function obj = SerialDependence(input)
            obj@Experiment(input, 'serialdependence');
                                    
            obj.input_handler = InputHandler();
            
            obj.stimulus = Grating();
            
            obj.data = prepare_data(obj);
            obj.n_trial = size(obj.data,1); 
            % assumes blocks all of equal length
            obj.n_trial_per_block = sum(obj.data.section == 1 & obj.data.block == 1); 
            
            % listen for entry in to new phase of trial
            addlistener(obj.stimulus, 'phase', 'PostSet',...
                @(src,event)obj.log_phase_change(src,event) );
        end        
                
        % methods defined in external files
        record_response(obj, ~, ~)
        record_task_event(obj,~,~)
        update_stimulus(obj, ~, ~)
        data = prepare_data(obj)
    end
    
    methods (Static)
        demographics(directory)        
    end
    
end

