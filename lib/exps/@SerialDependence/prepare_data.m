function data = prepare_data(obj)

data = readtable(fullfile(obj.input.root, 'task-serialdependence_blocking.csv'));
data = data((data.subject == obj.subject) & (data.session == obj.session), :);

data.isi_sec = max(data.isi_raw_sec - obj.window.slack*obj.window.ifi, 0);

end

