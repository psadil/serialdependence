function vbl = run(obj, window, varargin)
ip = inputParser;
addParameter(ip, 'vbl', GetSecs, @isnumeric);
parse(ip,varargin{:});
input = ip.Results;
vbl = input.vbl;

%% brief warning about upcoming trial

obj.fixation_rgb = obj.fixation_dim_rgb;
obj.draw_fixation(window);
[obj.last_vbl, obj.onset, obj.fliptime, obj.missed, obj.beam] = Screen('Flip', window.pointer, vbl);

%% target
% Draw the grating. It's defined such that we don't want any alpha
% blending going on
[sourceFactorOld, destinationFactorOld] = Screen('BlendFunction', ...
    window.pointer, 'GL_ONE', 'GL_ZERO');

Screen('DrawTexture', window.pointer, obj.tex, [], [],...
    convert_orientation_for_draw(obj.orientation), [], [], [], [], [], ...
    [obj.phase_deg, obj.freq_cpp, obj.contrast, 0]);

% return to original blending function
Screen('BlendFunction', window.pointer, sourceFactorOld, destinationFactorOld);

% Draw the noise on main window. with alpha blending implies getting an
% average signal
Screen('DrawTexture', window.pointer, obj.tex_noise, [], [], [], [], obj.noise_contrast);

% Overdraw the rectangular noise image with our special
% aperture image. The noise image will shine through in areas
% of the aperture image where its alpha value is zero (i.e.
% transparent):
Screen('DrawTexture', window.pointer, obj.aperture);

obj.draw_fixation(window);

% Let PTB know it can go ahead with preparing the stimulus for display
Screen('DrawingFinished', window.pointer);
obj.phase = 'warning';

% flip stimulus to screen after warning period has passed, blocking until
% that time
[obj.last_vbl, obj.onset, obj.fliptime, obj.missed, obj.beam] = Screen('Flip', window.pointer, obj.last_vbl + obj.warning_sec);

%% blank period

obj.draw_fixation(window);

% Let PTB know it can go ahead with preparing the stimulus for display
Screen('DrawingFinished', window.pointer);
obj.phase = 'target';
[obj.last_vbl, obj.onset, obj.fliptime, obj.missed, obj.beam] = Screen('Flip', window.pointer, obj.last_vbl + obj.target_sec);

%% response period

obj.draw_fixation(window);

% response cue
Screen('FrameOval', window.pointer, [0 0 0 1], obj.response_circ_rect, 4);

Screen('DrawingFinished', window.pointer);
obj.phase = 'blank';
[obj.last_vbl, obj.onset, obj.fliptime, obj.missed, obj.beam] = Screen('Flip', window.pointer, obj.last_vbl + obj.blank_sec);

SetMouse(window.xCenter, window.yCenter, window.pointer);
ShowCursor(window.pointer);
responding = true;
while responding
    
    [~, x, y] = GetClicks(window.pointer, 0);
    [responding, obj.response] = process_clicks(x, y, ...
        RectWidth(obj.response_circ_rect)/2, [window.xCenter; window.yCenter]);
end

%% feedback period

obj.draw_fixation(window);
Screen('DrawTexture', window.pointer, obj.tex_response, [], [], obj.response, [], [], ...
    [], [], kPsychDontDoRotation, [1, obj.feedback_sigma, obj.feedback_aspect_ratio, 0]);

Screen('DrawingFinished', window.pointer);
obj.phase = 'probe';

[obj.last_vbl, obj.onset, obj.fliptime, obj.missed, obj.beam] = Screen('Flip', window.pointer, GetSecs + obj.flip_now_slack*3);
HideCursor(window.pointer);

%% finalize
obj.fixation_rgb = obj.fixation_full_rgb;
obj.draw_fixation(window);

Screen('DrawingFinished', window.pointer);
obj.phase = 'feedback';

[obj.last_vbl, obj.onset, obj.fliptime, obj.missed, obj.beam] = Screen('Flip', window.pointer, obj.last_vbl + obj.feedback_sec);
vbl = obj.last_vbl;
obj.phase = 'isi';

end

function [responding, angle] = process_clicks(x, y, radius_pix, center_coords)

% how close does the click need to be to count
margin = 50;

% https://math.stackexchange.com/questions/127613/closest-point-on-circle-edge-from-point-outside-inside-the-circle
coord_on_unit_circle = ([x; y] - center_coords) / norm([x; y] - center_coords);
coord_on_response_circle = center_coords + radius_pix * coord_on_unit_circle;

% in pixel space, y goes backwards (top is 0, bottom is higher). need to
% invert to get useful unit circle representation for calculating angles
coord_on_unit_circle(2) = -1 * coord_on_unit_circle(2);

if pdist([coord_on_response_circle'; [x, y]]) < margin
    responding = false;
    angle = atan2d(coord_on_unit_circle(2), coord_on_unit_circle(1));
else
    responding = true;
    angle = NaN;
end

end

function out = convert_orientation_for_draw(ang)
% orientations are recorded like for unit circles

if ang <= 90
    out = angle_diff(ang, 90);
elseif ang <= 180
    out = -1 * angle_diff(ang, 90);
end

end
