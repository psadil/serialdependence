function vbl = prepare(obj, window)

%% timing 

% defines the quickest flip that will be possible
obj.flip_now_slack = window.slack * window.ifi;

obj.warning_sec = obj.warning_raw_sec - obj.flip_now_slack;
obj.target_sec = obj.target_raw_sec - obj.flip_now_slack;
obj.blank_sec = obj.blank_raw_sec - obj.flip_now_slack;
obj.feedback_sec = obj.feedback_raw_sec - obj.flip_now_slack;

%% size

% bunches of conversions from size in degrees to size in pixels
obj.radius_pix = window.deg2pix(obj.radius_deg);
obj.freq_cpp = (1 / window.deg2pix(1/obj.freq_cpd));

obj.fixation_pix = window.deg2pix(obj.fixation_deg);

% this is the main stimulus that will be drawn
[obj.tex, obj.gratingrect] = CreateProceduralSineGrating(window.pointer, ...
    obj.width_pix, obj.height_pix, obj.backgroundColorOffset, obj.radius_pix, ...
    obj.contrastPreMultiplicator);

obj.aperture_rect = SetRect(0,0, floor(obj.radius_pix*2), floor(obj.radius_pix*2));

response_circ_pix = window.deg2pix(obj.response_circ_deg);
obj.response_circ_rect = CenterRectOnPointd(SetRect(0, 0, floor(response_circ_pix), ...
    floor(response_circ_pix)), window.xCenter, window.yCenter);

% feedback bar
obj.tex_response = CreateProceduralGaussBlob(window.pointer, ...
    obj.width_pix, obj.height_pix, obj.backgroundColorOffset, 1, 0.5);

obj.feedback_sigma = RectWidth(obj.response_circ_rect)/5;

% Build a nice aperture texture: Offscreen windows can be used as
% textures as well, so we open an Offscreen window of exactly the same
% size 'objRect' as our noise textures, with a gray default background.
% This way, we can use the standard Screen drawing commands to 'draw'
% our aperture:
obj.aperture = Screen('OpenOffscreenWindow', window.pointer, window.background, obj.aperture_rect);

% noies image for now. exact set of pixels will tend to get overwritten on
% each trial
noiseimg = rand(floor(obj.radius_pix*2), floor(obj.radius_pix*2));
obj.tex_noise = Screen('MakeTexture', window.pointer, noiseimg);

% First we clear out the alpha channel of the aperture disk to zero -
% In this area the noise stimulus will shine through:
Screen('FillOval', obj.aperture, [1 1 1 0], obj.aperture_rect);

% Draw the grating once, just to make sure the gfx-hardware is ready, and
% the mex file loaded
Screen('DrawTexture', window.pointer, obj.tex, [], [],...
    obj.orientation, [], [], [], [], [], [obj.phase_deg, obj.freq_cpp, 0, 0]);

% Perform initial flip to prepare grating
vbl = Screen('Flip', window.pointer, GetSecs + obj.flip_now_slack*9);

obj.prepared = true;

end