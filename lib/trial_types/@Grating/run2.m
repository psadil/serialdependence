function vbl = run2(obj, window, varargin)
ip = inputParser;
addParameter(ip, 'vbl', GetSecs, @isnumeric);
addParameter(ip, 'diff', 10, @isnumeric);
parse(ip,varargin{:});
input = ip.Results;
vbl = input.vbl;

oris = 45 + [-input.diff/2, input.diff/2];

%% target
% Draw the grating. It's defined such that we don't want any alpha
% blending going on
[sourceFactorOld, destinationFactorOld] = Screen('BlendFunction', ...
    window.pointer, 'GL_ONE', 'GL_ZERO');

offset = deg2pix(window, 6);

Screen('DrawTextures', window.pointer, obj.tex, [], ...
    CenterRectOnPoint(obj.gratingrect, window.xCenter + [offset; -offset], [window.yCenter; window.yCenter] )',...
    oris, [], [], [], [], [], ...
    repmat([obj.phase_deg, obj.freq_cpp, obj.contrast, 0], 2, 1)');

% return to original blending function
Screen('BlendFunction', window.pointer, sourceFactorOld, destinationFactorOld);

% Draw the noise on main window. with alpha blending implies getting an
% average signal
noise_rect = SetRect(0,0,obj.radius_pix*2, obj.radius_pix*2);
Screen('DrawTextures', window.pointer, obj.tex_noise, [], ...
     CenterRectOnPoint(noise_rect, window.xCenter + [offset; -offset], [window.yCenter; window.yCenter] )', ...
    [], [], obj.noise_contrast);

% Overdraw the rectangular noise image with our special
% aperture image. The noise image will shine through in areas
% of the aperture image where its alpha value is zero (i.e.
% transparent):
Screen('DrawTextures', window.pointer, obj.aperture, [], ...
    CenterRectOnPoint(obj.aperture_rect, window.xCenter + [offset; -offset], [window.yCenter; window.yCenter] )');

obj.draw_fixation(window);

% Let PTB know it can go ahead with preparing the stimulus for display
Screen('DrawingFinished', window.pointer);

% flip stimulus to screen after warning period has passed, blocking until
% that time
vbl = Screen('Flip', window.pointer, vbl + obj.flip_now_slack);

GetClicks;

end

