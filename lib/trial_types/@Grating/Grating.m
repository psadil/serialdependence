classdef Grating < handle
    
    properties (Constant)
        width_pix = 2^8
        height_pix = 2^8
        backgroundColorOffset = [.5 .5 .5 0]
        
        contrastPreMultiplicator = 0.5
        radius_deg = 2
        freq_cpd = 1.5 % cycles per degree
        response_circ_deg = 6
        feedback_aspect_ratio = 10
        
        % hz determined by fastest transition, which in this case is target
        % presentation
        warning_raw_sec = 0.3
        target_raw_sec = 0.030
        blank_raw_sec = .6
        
        % the following times would be used  if refresh rate == 144hz
        %         warning_raw_sec = 44/144
        %         target_raw_sec = 5/144
        %         blank_raw_sec = 87/144
        feedback_raw_sec = 0.5
        
        fixation_deg = 0.08
        fixation_full_rgb = [1; 1; 1] % white
        fixation_dim_rgb = [.8; .8; .8] % gray
        
        phase_deg = 0
        noise_contrast = 0.5
        
    end
    
    properties
        % when prepared is set, each of the following textures will be
        % created. delete method will close them all
        prepared = false
        tex
        gratingrect
        tex_noise
        aperture
        tex_response
        
        warning_sec
        target_sec
        blank_sec
        feedback_sec
        
        % to calculate based on window size
        radius_pix
        aperture_rect
        freq_cpp % cycles per pixel
        response_circ_rect
        feedback_sigma
        
        fixation_rgb = [1; 1; 1]
        fixation_pix
        flip_now_slack
        
        % info to log after flips
        last_vbl
        onset
        fliptime
        missed
        beam
        
        % properties that can change trial-by-trial
        orientation = 0
        contrast = 0.1
        contrast_db = 0
        response
    end
    
    properties (SetObservable, AbortSet)
        % experiment objects will listen for changes to these
        phase
    end
    
    methods
        function obj = Grating()
        end
        
        function delete(obj)
            if obj.prepared
                Screen('Close', [obj.aperture, obj.tex, obj.tex_noise, obj.tex_response])
                obj.aperture = [];
                obj.tex = [];
                obj.tex_noise = [];
            end
        end
        
        vbl = prepare(obj, window)
        vbl = run(obj, window, varargin)
        draw_fixation(obj, window)
        
    end
    
    methods (Static)
        % do_nothing function comes up in a few places. E.g., input_handler
        % during run functions might call do_nothing when there is no
        % attached input_handler (as in the case of demos)
        function [] = do_nothing(varargin)
        end
    end
    
    
end

