function draw_fixation(obj, window)

Screen('DrawDots', window.pointer, [window.xCenter, window.yCenter], ...
    obj.fixation_pix, obj.fixation_rgb, [], 2);

end