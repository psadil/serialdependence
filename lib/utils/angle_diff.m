function abs_diff_deg = angle_diff(a, b)

% https://stackoverflow.com/questions/32276369/calculating-absolute-differences-between-two-angles
normalized_deg = mod(a-b, 180);
abs_diff_deg = min(180 - normalized_deg, normalized_deg);
        
end
