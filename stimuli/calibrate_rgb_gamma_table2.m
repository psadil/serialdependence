
iccprofile = iccread('vg248qe-rtings-icc-profil.icm');

readings = double([iccprofile.MatTRC.RedTRC,iccprofile.MatTRC.GreenTRC, iccprofile.MatTRC.BlueTRC]);
intensity_vals = linspace(0,1,size(readings,1))';

% fit the gamma and spline models - importantly, fit the constant term for use in illuminance-normalization

displayGamma = zeros(1,3);
displayConstant = zeros(1,3);
gammaTable = zeros(256,3);
displaySplineModel = zeros(256,3);

for channel = 1:3
    
    %Substract baseline (background) illuminance. Do not normalize.
    displayBaseline = min(readings(:,channel));
    chan_vals = (readings(:,channel) - displayBaseline);
    
    %Gamma function fitting
    fo = fitoptions('a*(x^g)','Lower',[0,1]);
    g = fittype('a*(x^g)','options',fo);
    fittedmodel = fit(intensity_vals,chan_vals,g);
    displayGamma(channel) = fittedmodel.g;
    displayConstant(channel) = fittedmodel.a;
    gammaTable(:,channel) = ((([0:255]'/255))).^(1/fittedmodel.g); %#ok<NBRAK>
    
    firstFit = fittedmodel([0:255]/255); %#ok<NBRAK>
    
    %Spline interp fitting
    fittedmodel = fit(intensity_vals,chan_vals,'splineinterp');
    displaySplineModel(:,channel) = fittedmodel([0:255]/255); %#ok<NBRAK>
    
    figure;
    plot(255*intensity_vals', chan_vals', '.', [0:255], firstFit, '--', [0:255], displaySplineModel(:,channel), '-.'); %#ok<NBRAK>
    legend('Measures', 'Gamma model', 'Spline interpolation');
    title(sprintf('Gamma model x^{%.2f} vs. Spline interpolation', displayGamma(channel)));
    
end

save('gammaTable-asus.mat','gammaTable')

% save the models
% save(['gammaTable-',monitor,'-rgb'],'gammaTable')
% save(['gammaFit-',monitor],'displayGamma','displayConstant','readings','intensity_vals')

