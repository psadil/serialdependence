function d = shufflenorepeats(k,n)
% https://stackoverflow.com/questions/34004818/matlab-help-shuffling-a-predefined-vector-without-consecutively-repeating-numbe
d=42; %// random number to fail first check
while(~all(sum(bsxfun(@eq,d,(1:n).'),2)==k)) %' //Check all numbers to appear k times.
    d=mod(cumsum([randi(n,1,1),randi(n-1,1,(n*k)-1)]),n)+1; %generate new random sample, enforcing a difference of at least 1.
end
