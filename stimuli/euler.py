# super rough script from https://gregorulm.com/finding-an-eulerian-path/
# used to generate random walks such that each orientation is reached once
# (and the specification of edges means that it is reached once from each 
# of the desired edges, too)

import itertools
import random
import pandas as pd

random.seed(122020)
n_list = 100 

def printCircuit(adj, curr_v):   
    # adj represents the adjacency list of 
    # the directed graph 
    # edge_count represents the number of edges 
    # emerging from a vertex 
    edge_count = dict()   
    for i in range(len(adj)):   
        # find the count of edges to keep track 
        # of unused edges 
        edge_count[i] = len(adj[i])   
    # Maintain a stack to keep vertices 
    curr_path = []   
    # vector to store final circuit 
    circuit = [] 
    # start from given node
    curr_path.append(curr_v) 
    while len(curr_path):   
        # If there's remaining edge 
        if edge_count[curr_v]: 
            # Push the vertex 
            curr_path.append(curr_v)   
            # Find the next vertex using an edge 
            next_v = adj[curr_v][-1] 
            # and remove that edge 
            edge_count[curr_v] -= 1
            adj[curr_v].pop() 
            # Move to next vertex 
            curr_v = next_v 
        # back-track to find remaining circuit 
        else: 
            circuit.append(curr_v)   
            # Back-tracking 
            curr_v = curr_path[-1] 
            curr_path.pop()   
    circuit.reverse()
    return(circuit)


def make_pairs(n_ori=18, max_diff = 4):
    pairs0 = list(itertools.product(range(0, n_ori*2+1), repeat=2))

    # keep only pairs that are close enough together
    pairs = []
    for x in pairs0:        
        if ( (0 in x) |
            (all([y <= n_ori for y in x]) & ((abs(x[0] - x[1]) <= max_diff) | (abs(x[0] - x[1]) >= (n_ori - max_diff))) ) | 
            (all([y >  n_ori for y in x]) & ((abs(x[0] - x[1]) <= max_diff) | (abs(x[0] - x[1]) >= (n_ori - max_diff))) ) |
            (((max(x) > n_ori) & (min(x) <= n_ori)) & ((abs(max(x) - n_ori - min(x)) <= max_diff) | (abs(max(x) - n_ori - min(x)) >= (n_ori - max_diff))) ) ):
             pairs.append(x)

    adj = [0] * (n_ori*2+1)
    for i in range((n_ori*2+1)): 
        adj[i] = [] 
    for p in pairs:
        adj[p[0]].append(p[1])

    return(adj)

out = []
for i in range(n_list):
    adj = make_pairs()
    #random.shuffle(pairs)
    out.append(printCircuit(adj, random.choice(range(1,19))))

block = [[x]*len(out[0]) for x in range(n_list)]
blocks = [item for sublist in block for item in sublist]
orientations = [item for sublist in out for item in sublist]

d = {'blocks': blocks, 'orientations': orientations}

out = pd.DataFrame(d)
out.to_csv('orientations.csv', index=False)

