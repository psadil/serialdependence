% run script to create blocking. note that the transitions have been
% predetermined by the script, euler.py. That script creates blocks in
% which the transitions are counterbalanced after 722 trials. However, that
% would be too many trials without a break. Instead, the transitions are
% broken up into chunks, such that a transition which was ...->A->B->C->...
% becomes ...->A->B | B->C->... This requires adding an extra trial for
% each new block. The number of blocks per session is determined by
% something that allows all blocks to have equal length. 

seed = 12312019;
rng(seed);

n_subject = 10;
n_session = 3;
n_section_per_block_block = 7;
n_block = 2;
n_section = n_section_per_block_block*n_block;
n_per_full_block = 722;
n_per_section = (n_per_full_block+(n_section_per_block_block-1)) / n_section_per_block_block;
n = n_per_section*n_section;

n_ori = 18;
orientations_to_use = linspace(0, 180 - (180/n_ori), n_ori);
contrasts_db = [-Inf, -30, -20];

preset = readtable('stimuli/orientations.csv');

T = table();

for subject = 1:n_subject
    for ses = 1:n_session
        t = table();
        t.subject = repelem(subject, n)';
        t.session = repelem(ses, n)';
        t.block = repelem(1:n_block, (n / n_block))';
        t.section = repmat(repelem(1:n_section_per_block_block, n_per_section)', [n_block, 1]);
        t.trial = repmat(1:n_per_section, [1, n_section])';
        

        trial_type = [];
        for block = 0:(n_block-1)
            trial_type_raw = preset.orientations(preset.blocks == block);

            trial = 1;
            for section = 1:n_section_per_block_block
                trial_type = [trial_type; trial_type_raw(trial:(trial + n_per_section-1))]; %#ok<AGROW>
                trial = trial + (n_per_section-1);
            end
        end
        t.trial_type = trial_type;
                
        orientation = trial_type;
        orientation(orientation > n_ori) = orientation(orientation > n_ori) - n_ori;
        orientation(orientation > 0) = orientations_to_use(orientation(orientation > 0));
            
        % note that we leave orientation of 0 contrast stimuli as 0 to
        % avoid breaking psychtoolbox (even though orientation is really NaN)
        t.orientation = orientation;

        contrast_db = zeros(n, 1);
        contrast_db(trial_type <= n_ori) = contrasts_db(2);
        contrast_db(trial_type > n_ori) = contrasts_db(3);
        contrast_db(trial_type == 0)  = contrasts_db(1);
        t.contrast_db = contrast_db;
        
        % isi refers to previous trial. So, it'll be the amount of time that
        % preceeded this stimulus (i.e., the first trial starts right away)
        t.isi_raw_sec = (randsample(0:20:200, n, true)' + 300)/1000;
        t.isi_raw_sec(1) = 0;
        
        T = [T; t]; %#ok<AGROW>        
    end    
end

T.contrast = 10 .^ (T.contrast_db ./ 20);
T.warning_on = NaN(size(T,1),1);
T.target_on = NaN(size(T,1),1);
T.blank_on = NaN(size(T,1),1);
T.probe_on = NaN(size(T,1),1);
T.feedback_on = NaN(size(T,1),1);
T.isi_on = NaN(size(T,1),1);
T.response = NaN(size(T,1),1);
T.rt = NaN(size(T,1),1);

% extra verbose logging
T.warning_onset = NaN(size(T,1),1);
T.target_onset = NaN(size(T,1),1);
T.blank_onset = NaN(size(T,1),1);
T.probe_onset = NaN(size(T,1),1);
T.feedback_onset = NaN(size(T,1),1);
T.isi_onset = NaN(size(T,1),1);

T.warning_fliptime = NaN(size(T,1),1);
T.target_fliptime = NaN(size(T,1),1);
T.blank_fliptime = NaN(size(T,1),1);
T.probe_fliptime = NaN(size(T,1),1);
T.feedback_fliptime = NaN(size(T,1),1);
T.isi_fliptime = NaN(size(T,1),1);

T.warning_missed = NaN(size(T,1),1);
T.target_missed = NaN(size(T,1),1);
T.blank_missed = NaN(size(T,1),1);
T.probe_missed = NaN(size(T,1),1);
T.feedback_missed = NaN(size(T,1),1);
T.isi_missed = NaN(size(T,1),1);

T.warning_beam = NaN(size(T,1),1);
T.target_beam = NaN(size(T,1),1);
T.blank_beam = NaN(size(T,1),1);
T.probe_beam = NaN(size(T,1),1);
T.feedback_beam = NaN(size(T,1),1);
T.isi_beam = NaN(size(T,1),1);
T.seed = repelem(seed, size(T,1))';
T.date = repelem(datetime, size(T,1))';

writetable(T,'task-serialdependence_blocking.csv');
