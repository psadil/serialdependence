
rng(04122019);

% ----- wedgering trials -----
n_block = 10;
n_subject = 10;
n_stim_repeats = 100;
n_contrast = 10;
n = n_stim_repeats*n_contrast;
n_trial_per_block = n / n_block;
n_repeats_per_block = n_trial_per_block / n_contrast;

T = table();

for subject = 1:n_subject
    t = table();
    t.trial = (1:n)';
    t.orientation = randsample(1:179, n, true)';
    t.subject = repelem(subject, n)';
    
    % isi refers to previous trial. So, it'll be the amount of time that
    % preceeded this stimulus (i.e., the first trial starts right away)
    t.isi_raw_sec = (randsample(0:20:200, n, true)' + 300)/1000;
    t.isi_raw_sec(1) = 0;
    
    contrast_db = [];
    for block = 1:n_block
       contrast_db = [contrast_db; Shuffle(repelem(linspace(-40,-20,n_contrast), n_repeats_per_block)')]; %#ok<AGROW>
    end
    
    % filled in during stimulus presentation
    % contrast is contrast of grating before averaging (noise always full)
    t.contrast_db = contrast_db;
    T = [T; t]; %#ok<AGROW>
end

T.block = ceil(T.trial / n_trial_per_block);
T.contrast = 10 .^ (T.contrast_db ./ 20);
T.warning_on = NaN(size(T,1),1);
T.target_on = NaN(size(T,1),1);
T.blank_on = NaN(size(T,1),1);
T.probe_on = NaN(size(T,1),1);
T.feedback_on = NaN(size(T,1),1);
T.isi_on = NaN(size(T,1),1);
T.response = NaN(size(T,1),1);
T.rt = NaN(size(T,1),1);
T.error_deg = NaN(size(T,1),1);
T.correct = NaN(size(T,1),1);

% extra verbose logging
T.warning_onset = NaN(size(T,1),1);
T.target_onset = NaN(size(T,1),1);
T.blank_onset = NaN(size(T,1),1);
T.probe_onset = NaN(size(T,1),1);
T.feedback_onset = NaN(size(T,1),1);
T.isi_onset = NaN(size(T,1),1);

T.warning_fliptime = NaN(size(T,1),1);
T.target_fliptime = NaN(size(T,1),1);
T.blank_fliptime = NaN(size(T,1),1);
T.probe_fliptime = NaN(size(T,1),1);
T.feedback_fliptime = NaN(size(T,1),1);
T.isi_fliptime = NaN(size(T,1),1);

T.warning_missed = NaN(size(T,1),1);
T.target_missed = NaN(size(T,1),1);
T.blank_missed = NaN(size(T,1),1);
T.probe_missed = NaN(size(T,1),1);
T.feedback_missed = NaN(size(T,1),1);
T.isi_missed = NaN(size(T,1),1);

T.warning_beam = NaN(size(T,1),1);
T.target_beam = NaN(size(T,1),1);
T.blank_beam = NaN(size(T,1),1);
T.probe_beam = NaN(size(T,1),1);
T.feedback_beam = NaN(size(T,1),1);
T.isi_beam = NaN(size(T,1),1);


writetable(T,'task-threshold_blocking.csv');

