
rng(04122019);

n = 300;
n_trial_per_block = 100;
n_subject = 20;

T = table();

for subject = 1:n_subject
    t = table();
    t.trial = (1:n)';
    t.orientation = randsample(1:179, n, true)';
    t.subject = repelem(subject, n)';
    
    % isi refers to previous trial. So, it'll be the amount of time that
    % preceeded this stimulus (i.e., the first trial starts right away)
    t.isi_raw_sec = (randsample(0:20:200, n, true)' + 300)/1000;
    t.isi_raw_sec(1) = 0;
    
    % filled in during stimulus presentation
    % contrast is contrast of grating before averaging (noise always full)
    t.contrast_db = NaN(size(t,1),1);
    t.contrast_db(1) = 20*log10(.05);    
    T = [T; t];
end

T.block = ceil(T.trial / n_trial_per_block);
T.contrast = 10 .^ (T.contrast_db ./ 20);
T.warning_on = NaN(size(T,1),1);
T.target_on = NaN(size(T,1),1);
T.blank_on = NaN(size(T,1),1);
T.probe_on = NaN(size(T,1),1);
T.feedback_on = NaN(size(T,1),1);
T.isi_on = NaN(size(T,1),1);
T.response = NaN(size(T,1),1);
T.rt = NaN(size(T,1),1);
T.error_deg = NaN(size(T,1),1);
T.correct = NaN(size(T,1),1);

writetable(T,'task-staircase_blocking.csv');


