
% just trying a simple version first, ignoring contrast
x = debruijn_generator(18, 2);

% now, need to eliminate every transition that is too large
angles = linspace(-180, 180 - (360/18), 18);
xx = angles(x);
diffs = angdifference(xx(2:end), xx(1:(end-1)));
yy = xx(abs([0, diffs]) < 100);
newdiffs = angdifference(yy(2:end), yy(1:(end-1)));

x = perms(1:7);
xx = x(all(abs(diff(x, 1, 2)) < 3, 2),:);


g1 = repmat([0,0,1,0,0, 0,0,0,1,0, 0,0,0,0,1, 1,0,0,0,0, 0,1,0,0,0],[5,1]);
g2 = repmat([0,1,0,0,0, 0,0,1,0,0, 0,0,0,1,0, 0,0,0,0,1, 1,0,0,0,0], [5,1]);
g3 = repmat([1,0,0,0,0, 0,1,0,0,0, 0,0,1,0,0, 0,0,0,1,0, 0,0,0,0,1], [5,1]);
g4 = repmat([0,0,0,0,1, 1,0,0,0,0, 0,1,0,0,0, 0,0,1,0,0, 0,0,0,1,0], [5,1]);
g5 = repmat([0,0,0,1,0, 0,0,0,0,1, 1,0,0,0,0, 0,1,0,0,0, 0,0,1,0,0], [5,1]);

g = [g1; g2; g3; g4; g5];
source = 1;
destination = 25;

out = hamiltonian(g, source, destination);
