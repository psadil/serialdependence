% this script constructs a counterbalanced sequence, but the
% counterbalancing may be misguided. The trials are balanced such that
% orientation differences follow eachother equally often. Orientation
% differences might not matter. It might be more important that each
% _raw orientation_ is preceeded by each difference equally often. This is
% the balancing that Fisher & Whitney described, whereby e.g., a 100 degree
% orientation was preceeded by 60, 70, 80, 90, 100, 110, 120, 130, and 140
% degree. 


seed = 12312019;
rng(seed);

%
n_subject = 1;
n_session = 2;
n_block = 5;
n_orientation_diffs = 9;
n_contrast = 2;
n_trial_types = 19;
n_per_block = (n_trial_types^2 + 1);
n = n_per_block*n_block;

% orientation gets wrapped latter, so these starting oris need to be in
% range of [-180, 180]
starting_orientations = linspace(-180, 180 - (360/n_block), n_block);
orientation_diffs = linspace(-40,40,9);
contrasts_db = [-Inf, -30, -20];

T = table();

for subject = 1:n_subject
    for ses = 1:n_session
        t = table();
        t.subject = repelem(subject, n)';
        t.session = repelem(ses, n)';
        t.block = repelem(1:n_block, n_per_block)';
        t.trial = repmat(1:n_per_block, [1, n_block])';
        
        trial_types = [];
        orientation_diff_all = [];
        orientations = [];
        session_starting_oris = Shuffle(starting_orientations);
        for block = 1:n_block
            base = debruijn_generator(n_trial_types, 2);
            these_types = [base; base(1)];
            trial_types = [trial_types; these_types]; %#ok<AGROW>
            
            orientation_diff = zeros(n_per_block, 1);
            for ori = 1:n_orientation_diffs
                orientation_diff(these_types == ori) = orientation_diffs(ori);
                orientation_diff((these_types - n_orientation_diffs) == ori) = orientation_diffs(ori);
            end

            orientation = zeros(n_per_block, 1);
            orientation(1) = session_starting_oris(block);
            for trial = 2:n_per_block
                orientation(trial) = orientation(trial-1) + orientation_diff(trial);
            end
            orientation = wrapTo180(orientation);
            orientation(orientation < 0) = 180 - abs(orientation(orientation < 0));
            orientations = [orientations; orientation]; %#ok<AGROW>
            
            % orientation_diff is NaN on every first trial and every trial
            % following 0 contrast stimulus
            % however, the orientation diff following 0 contrast trial is
            % used to actually determine the orientations on said trial.
            % e.g., if trial 1 has ori 0 followed by 0 contrast followed by
            % trial type 10 (+10 degree rotation), then trial 3 will have
            % orientation 10 (if there is a contrast). The NaNs help ensure
            % furture analyses don't accidentally tally these trials
            orientation_diff([true; these_types(1:(end-1)) == 19]) = NaN;
            orientation_diff_all = [orientation_diff_all; orientation_diff];  %#ok<AGROW>            
        end
        t.trial_type = trial_types;        
        t.orientation_diff = orientation_diff_all;
        t.orientation = orientations;

        contrast_db = zeros(n, 1);
        contrast_db(trial_types <= n_orientation_diffs) = contrasts_db(2);
        contrast_db(trial_types > n_orientation_diffs) = contrasts_db(3);
        contrast_db(trial_types == 19)  = contrasts_db(1);
        t.contrast_db = contrast_db;
        
        % isi refers to previous trial. So, it'll be the amount of time that
        % preceeded this stimulus (i.e., the first trial starts right away)
        t.isi_raw_sec = (randsample(0:20:200, n, true)' + 300)/1000;
        t.isi_raw_sec(1) = 0;
        
        T = [T; t]; %#ok<AGROW>        
    end    
end

T.contrast = 10 .^ (T.contrast_db ./ 20);
T.warning_on = NaN(size(T,1),1);
T.target_on = NaN(size(T,1),1);
T.blank_on = NaN(size(T,1),1);
T.probe_on = NaN(size(T,1),1);
T.feedback_on = NaN(size(T,1),1);
T.isi_on = NaN(size(T,1),1);
T.response = NaN(size(T,1),1);
T.rt = NaN(size(T,1),1);

% extra verbose logging
T.warning_onset = NaN(size(T,1),1);
T.target_onset = NaN(size(T,1),1);
T.blank_onset = NaN(size(T,1),1);
T.probe_onset = NaN(size(T,1),1);
T.feedback_onset = NaN(size(T,1),1);
T.isi_onset = NaN(size(T,1),1);

T.warning_fliptime = NaN(size(T,1),1);
T.target_fliptime = NaN(size(T,1),1);
T.blank_fliptime = NaN(size(T,1),1);
T.probe_fliptime = NaN(size(T,1),1);
T.feedback_fliptime = NaN(size(T,1),1);
T.isi_fliptime = NaN(size(T,1),1);

T.warning_missed = NaN(size(T,1),1);
T.target_missed = NaN(size(T,1),1);
T.blank_missed = NaN(size(T,1),1);
T.probe_missed = NaN(size(T,1),1);
T.feedback_missed = NaN(size(T,1),1);
T.isi_missed = NaN(size(T,1),1);

T.warning_beam = NaN(size(T,1),1);
T.target_beam = NaN(size(T,1),1);
T.blank_beam = NaN(size(T,1),1);
T.probe_beam = NaN(size(T,1),1);
T.feedback_beam = NaN(size(T,1),1);
T.isi_beam = NaN(size(T,1),1);
T.seed = repelem(seed, size(T,1))';
T.date = repelem(datetime, size(T,1))';

writetable(T,'task-serialdependence_blocking.csv');
