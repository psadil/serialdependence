function a = angdifference(targetA, sourceA)

a = targetA - sourceA;
a(a > 180) = a(a > 180) - 360;
a(a < -180) = a(a < -180) + 360;

end

