function  main_threshold(varargin)
ip = inputParser;
addParameter(ip, 'subject', 99, @isnumeric);
addParameter(ip, 'debuglevel', 0, @(x) x == 1 | x == 10 | x == 0);
addParameter(ip, 'skipsynctests', 0, @(x) any(x == 0:2));
addParameter(ip, 'root', pwd, @ischar);
addParameter(ip, 'gamma', true, @islogical);
parse(ip,varargin{:});
input = ip.Results;
addpath(genpath('lib'));

% gather demographics for practice run
% if input.debuglevel == 0
%     demographics('events');
% end

%%  setup
PsychDefaultSetup(2);

% run staircase
staircase = Threshold(input);
staircase.run();
% delete to force save
delete(staircase);

rmpath(genpath('lib'));

end